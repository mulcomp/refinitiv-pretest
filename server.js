const express = require("express");
const app = express();
const Browser = require("zombie");
const cheerio = require("cheerio");
const url = "https://codequiz.azurewebsites.net/";

const port = 3080;
const scrapedData = [];
const args = process.argv.slice(2);

// app.get("/", (req, res) => {
//     res.send("Hello World")
// });

app.listen(port, () => {
  //console.log(`Server listening on the port::${port}`);

  let browser = new Browser();
  browser
    .visit(url)
    .then(() => {
      browser.pressButton("Accept").then(() => {
        const $ = cheerio.load(browser.html("table"));

        $("body > table > tbody > tr").each((index, element) => {
          if (index === 0) return true;
          const tds = $(element).find("td");
          const fundname = $(tds[0]).text();
          const nav = parseFloat($(tds[1]).text());
          const tableRow = { fundname, nav };

          scrapedData.push(tableRow);
        });

        //console.log(scrapedData);

        process.argv.slice(2).forEach(function (val) {
          var nav = scrapedData.find((x) => x.fundname === val);
          if (nav !== undefined) {
            console.log("Fund Name '" + val + "' NAV : ", nav.nav);
          } else {
            console.log("Fund Name '" + val + "' Not Found ");
          }
        });

      });
    })
    .catch((error) => {
      console.error(`Error occurred visiting ${url}`);
    });
});

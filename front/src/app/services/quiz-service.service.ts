import { Injectable } from '@angular/core';
import { catchError, finalize, map } from 'rxjs/operators';
import { Observable, of, from, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class QuizServiceService {
  constructor(public http: HttpClient) {}

  getQ2API(): Observable<any> {
    return this.http
      .get<any>('https://api.publicapis.org/categories', this.httpOptions)
      .pipe(
        catchError((err) => {
          console.log('Handling error locally and rethrowing it...', err);
          return throwError(err);
        }),
        finalize(() => console.log('finalize'))
      );
  }

  public get httpOptions(): any {
    return {
      responseType: 'json',
      observe: 'response',
    };
  }
}

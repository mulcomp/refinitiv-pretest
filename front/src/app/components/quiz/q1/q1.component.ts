import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-q1',
  templateUrl: './q1.component.html',
  styleUrls: ['./q1.component.scss'],
})
export class Q1Component implements OnInit {
  inputnumber: number = 1;
  result: string = 'true';
  selected: string = 'isPrime';

  constructor() {}

  ngOnInit(): void {}

  onInputChangeEvent(e: any) {
    this.inputnumber =
      isNaN(e.target.value) || parseInt(e.target.value) < 0
        ? 1
        : parseInt(e.target.value);

    this.result =
      this.selected === 'isPrime'
        ? this.isPrime(this.inputnumber).toString()
        : this.isFibonacci(this.inputnumber).toString();
  }
  onSelectChangeEvent(e: any) {
    //console.log('this.selected : ', this.selected);
    this.result =
      e.target.value === 'isPrime'
        ? this.isPrime(this.inputnumber).toString()
        : this.isFibonacci(this.inputnumber).toString();
  }

  isPrime(num: number) {
    for (var i = 2; i < num; i++) {
      if (num % i === 0) {
        return false;
        break;
      }
    }
    return true;
  }

  isFibonacci(num: number) {
    if (num === 0 || num === 1 || num === 2) {
      return true;
    }
    let prev = 1;
    let count = 2;
    let temp = 0;
    while (count <= num) {
      if (prev + count === num) {
        return true;
      }
      temp = prev;
      prev = count;
      count += temp;
    }
    return false;
  }
}

import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { QuizServiceService } from '../../../services/quiz-service.service';
@Component({
  selector: 'app-q2',
  templateUrl: './q2.component.html',
  styleUrls: ['./q2.component.scss'],
})
export class Q2Component implements OnInit {
  data: any;
  tmp: any;
  subscription: Subscription = new Subscription();

  constructor(private service: QuizServiceService) {}

  ngOnInit(): void {
    this.subscription = this.service.getQ2API().subscribe((res) => {
      if (res.ok) {
        this.data = res.body;
        this.tmp = [...this.data];
      }
    });
  }

  onCategoryFilterEvent(e: any) {
    if (e.target.value != '') {
      let str = e.target.value.toLowerCase();

      this.tmp = this.data.filter(
        (s: any) => s.toLowerCase().indexOf(str) > -1
      );

      this.tmp = [...this.tmp];
      //this.data.filter((item) => item === e.target.value);
    } else {
      this.tmp = [...this.data];
    }
  }

  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    if (this.subscription !== undefined) {
      this.subscription.unsubscribe();
    }
  }
}

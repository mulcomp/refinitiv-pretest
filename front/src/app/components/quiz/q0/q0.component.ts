import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-q0',
  templateUrl: './q0.component.html',
  styleUrls: ['./q0.component.scss'],
})
export class Q0Component implements OnInit {


  constructor() {}

  async ngOnInit() {
    const number1 = await this.thisIsSyncFunction().then((res) => {
      return res;
    });
    console.log('number1 : ', number1);
    const calculation = number1 * 10;
    console.log(calculation);
  }

  async thisIsSyncFunction() {
    return await fetch('https://codequiz.azurewebsites.net/data')
      .then((res) => res.json())
      .then((response) => {
        return response.data;
      });
  }
}

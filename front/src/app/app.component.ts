import { Component } from '@angular/core';
import { Injectable } from '@angular/core';
import { Observable, Observer } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'refinitiv-pretest';

  constructor() {}
}

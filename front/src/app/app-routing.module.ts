import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Q0Component } from './components/quiz/q0/q0.component';
import { Q1Component } from './components/quiz/q1/q1.component';
import { Q2Component } from './components/quiz/q2/q2.component';

const routes: Routes = [
  {
    path: 'q0',
    component: Q0Component,
  },
  {
    path: 'q1',
    component: Q1Component,
  },
  {
    path: 'q2',
    component: Q2Component,
  },
  {
    path: '',
    redirectTo: 'q0',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

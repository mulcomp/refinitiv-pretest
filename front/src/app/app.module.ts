import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { Q0Component } from './components/quiz/q0/q0.component';
import { Q1Component } from './components/quiz/q1/q1.component';
import { Q2Component } from './components/quiz/q2/q2.component';

@NgModule({
  declarations: [
    AppComponent,
    Q0Component,
    Q1Component,
    Q2Component,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
